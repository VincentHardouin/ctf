Jeu en ligne 
Utilisant un serveur permettant le lien entre 2 clients. 

Possibilité de créer un serveur, et à un autre joueur de rejoindre. 


**Règles :**
- CTF (Capture the flag)
- 3 captures = match gagné
- Temps de réapparition 
- Balles peut parcourir qu’un certain nombre de cases 
- Drapeau laissé au sol quand le porteur est tué et le joueur doit le récupérer pour qu’il retourne dans son camp 
-

**Fonctionnalités :**
- Jouer à 2
- Connexion à une base de données pour connaitre la liste des serveurs disponibles
- Créer son serveur 
- Se connecter à un serveur
- Gestion des Threads : resynchronisation, fin d'exécution 

**Utilises :**
- JDBC 
- Java socket 
- Thread
- JavaFX 

**Sructure :**
- Connect : Singleton 
- Semblant de MVC
- 

**Nécessaires :**
- JDBC 
- Mettre en place le dump 
- Changer les valeurs de Connect pour vous connecter à votre base de données 
- 
