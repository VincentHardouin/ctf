package ctf.elementjeu;

import java.io.File;

import ctf.Coord;
import javafx.animation.TranslateTransition;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class Flag extends AbstractElementJeu {

	// An image file on the hard drive.
	File file = new File("flag.png");

	// --> file:/C:/MyImages/myphoto.jpg
	String localUrl = file.toURI().toString();

	Image image = new Image(localUrl,20,20,false,true);



	public Flag(int x, int y, Pane pane) {
		coord = new Coord(x, y);
		node = new ImageView(image);

		node.setId("joueur");
		positionnement(pane);
	}


	public Coord getCoord() {
		return new Coord(coord.getX(), coord.getY());
	}

	public void setCoord(Coord coord) {
		this.coord = coord;
		if (coord == null) {
			node.setVisible(false);
		} else {
			node.setVisible(true);
			TranslateTransition tt = new TranslateTransition(DureeAnimation, node);
			tt.setToX(coord.getX() * width);
			tt.setToY(coord.getY() * width);
			tt.play();
		}
	}
	
	public void setVisible(boolean visible) {
		node.setVisible(visible);
	}


	public boolean estCache() {
		return this.coord == null;
	}





}


