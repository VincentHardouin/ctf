package ctf.elementjeu;

import java.io.ByteArrayInputStream;
import java.io.File;

import ctf.Coord;
import javafx.animation.TranslateTransition;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class Joueur extends AbstractElementJeu {
	private int idJoueur;
	private String nomJoueur;
	private String email;
	private String password;
	private String token;
	private int score;
	private boolean porteur; 


	private static String imageJoueurAsHexBinary = "47494638376114001400a102000000007f7f7fffffffffffff2c000000001400140000022f948fa90bbd2fc09110d159b1cde676fe8560e48c9a29a25ed6b4aeb98e3138b3a51c0841ad00b9b1e3f8124114a400003b";


	static File file = new File("perso.png"); 

	static String localUrl = file.toURI().toString();

	private static Image imagePorteur = new Image(localUrl,20,20,false,true);

	private static Image imageJoueur = new Image(
			new ByteArrayInputStream(hexStringToByteArray(imageJoueurAsHexBinary))
			);

	private static byte[] hexStringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len/2];

		for(int i = 0; i < len; i+=2){
			data[i/2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i+1), 16));
		}

		return data;
	}

	public Joueur(int x, int y, Pane pane) {
		porteur = false;
		coord = new Coord(x, y);
		node = new ImageView(imageJoueur);
		node.setId("joueur");
		positionnement(pane);
	}
	public Joueur() {

	}


	public Joueur(String nomJoueur, String email, String password) {
		this.nomJoueur = nomJoueur;
		this.email = email;
		this.password = password;
	}

	public Joueur(int idJoueur, String password) {
		this.idJoueur = idJoueur;
		this.password = password;
	}


	public Joueur(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}

	public Coord getCoord() {
		return new Coord(coord.getX(), coord.getY());
	}

	public int getIdJoueur() {
		return idJoueur;
	}

	public void setIdJoueur(int idjoueur) {
		this.idJoueur = idjoueur;
	}

	public String getNomJoueur() {
		return nomJoueur;
	}

	public void setNomJoueur(String nomJoueur) {
		this.nomJoueur = nomJoueur;
	}


	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}


	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}

	/**
	 * @param score the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}

	public void setCoord(Coord coord) {
		this.coord = coord;
		if (coord == null) {
			node.setVisible(false);
		} else {
			node.setVisible(true);
			TranslateTransition tt = new TranslateTransition(DureeAnimation, node);
			tt.setToX(coord.getX() * width );
			tt.setToY(coord.getY() * width );
			tt.play();
		}
	}

	public boolean isPorteur() {
		return porteur;
	}

	public void setPorteur(boolean porteur) {
		this.porteur = porteur;
		if(this.porteur) {
			((ImageView) node).setImage(imagePorteur);

		} else {
			((ImageView) node).setImage(imageJoueur);
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Joueur [idJoueur=" + idJoueur + ", nomJoueur=" + nomJoueur + ", email=" + email + ", password="
				+ password + ", token=" + token + ", score=" + score + "]";
	}


}


