package ctf.elementjeu;


import java.io.File;

import ctf.Coord;
import ctf.action.KillJoueur;
import ctf.action.KillJoueurAdverse;
import javafx.animation.TranslateTransition;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

public class Balle extends AbstractElementJeu {

	// An image file on the hard drive.
	File file = new File("balle.png");

	// --> file:/C:/MyImages/myphoto.jpg
	String localUrl = file.toURI().toString();

	Image image = new Image(localUrl,20,20,false,true);

	int nbrCaseDeplacement; 
	boolean killJoueur; 
	boolean killJoueurAdverse; 
	
	public Balle(int x, int y, Pane pane) {
		coord = new Coord(x, y);
		node = new ImageView(image);

		node.setId("joueur");
		positionnement(pane);
	}
	
	public Balle(Coord c,int direction,int nbrCaseDeplacement,boolean killJoueur, boolean killJoueurAdverse, Pane pane) {
		coord = c;
		this.killJoueur = killJoueur;
		this.killJoueurAdverse = killJoueurAdverse;
		
		this.nbrCaseDeplacement = nbrCaseDeplacement;
		
		node = new ImageView(image);
		node.setId("balle");
		
		switch(direction) {
		case 2: 
			node.setRotate(90);
			break;
		case 3: 
			node.setRotate(180);
			break;
		case 4: 
			node.setRotate(-90);
			break;
		default: 
			break;
		}
		
		
		positionnement(pane);
	}

	public Coord getCoord() {
		return new Coord(coord.getX(), coord.getY());
	}

	public void setCoord(Coord coord) {
		this.coord = coord;
		if (coord == null) {
			node.setVisible(false);
		} else {
			node.setVisible(true);
			Duration DureeAnimationBalle = Duration.millis(170*nbrCaseDeplacement);
			TranslateTransition tt = new TranslateTransition(DureeAnimationBalle, node);
			tt.setToX(coord.getX() * width);
			tt.setToY(coord.getY() * width);
			tt.play();
			tt.onFinishedProperty().set(e -> {
				this.setCoord(null);
				if(killJoueurAdverse) {
					KillJoueurAdverse killJ = new KillJoueurAdverse(); 
					Thread t = new Thread(killJ);
					t.start();
				} else if(killJoueur) {
					KillJoueur killJ = new KillJoueur(); 
					Thread t = new Thread(killJ);
					t.start();
				}
			});;
			
		}
	}
	
	public void setVisible(boolean visible) {
		node.setVisible(visible);
	}


	public boolean estCache() {
		return this.coord == null;
	}





}


