package ctf;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import connexion.ConnexionServer;
import connexion.GestionRqt;
import connexion.GestionToken;
import connexion.Hashage;
import connexion.JoueurAuth;
import connexion.LoginJoueur;
import connexion.VerifExist;
import ctf.elementjeu.Joueur;


/**
 * Controller de Jeu sokoban
 *
 */
public class JeuCtfController {
	
	private GestionRqt g;
	private ConnexionServer co;

	
	public JeuCtfController(String ipLocal, String ipExt) throws SQLException, IOException {
		double debut = System.nanoTime(); 
		g = new GestionRqt();
		co = new ConnexionServer(ipLocal, ipExt);
		double fin = System.nanoTime(); 
		
		System.out.println("Temps constructeur controller : " + (fin-debut)/1e9 + "sec" );

	}
	
	/**
	 * Permets de savoir si l'utilisateur est connecté ou non
	 * @return booloean : true si oui; false sinon
	 */
	public boolean isConnect() {
		JoueurAuth ja = new JoueurAuth();
		if (ja.getJoueurAuth() != null)
			return true;
		return false;
	}

	/**
	 * Permets de savoir si une progression a été sauvegardé ou non 
	 * @return boolean :
	 */
	public boolean haveSave() {
		if(isConnect()) {
			Niveau n = g.getSave(); 
			if(n.getScore() != 0) return true;
			return false;
		}
		return false;
	}
	
	/**
	 * Permets de sauvegarder la progression
	 * @param n : Niveau
	 */
	public void saveProgression(Niveau n) {
		g.setScoreAndSave(n);
	}
	
	
	/**
	 * Permets de sauvegarder le score 
	 * @param n : Niveau
	 */
	public void saveScore(Niveau n) {
		g.setScore(n);
		System.out.println("Votre score est sauvegarde");
	}
	
	/**
	 * Retourne la sauvegarde du joueur authentifié
	 * @return Niveau
	 */
	public Niveau getSauvegarde() {
		return g.getSave();
	}
	
	/**
	 * Permets de verifier si le nouveau score est mieux que l'ancien enregistré
	 * @param niveau : int : niveau dont on souhaite l'ancien score
	 * @param nbrDeplacement : score actuel
	 * @return boolean : true si le score est mieux que le précédent; false sinon
	 */
	public boolean haveBetterScore(int niveau, int nbrDeplacement) {
		if(isConnect()) {
			int oldScore = getOldScore(niveau);
			if(oldScore != 0) {
				if(oldScore > nbrDeplacement) return true;
				else return false;
			} else return false;
		} else return false;
	}
	
	
	/**
	 * Permets de connaitre le score déjà enregistré dans la base de données 
	 * pour le joueurAuthentifié en fournissant en paramètre le niveau pour lequel on souhaite le score
	 * @param niveau : int 
	 * @return int : score enregistré
	 */
	public int getOldScore(int niveau) {
		Joueur j = g.getJoueurScoreByLevel(niveau);
		return j.getScore();
	}
	
		
	/**
	 * Permets d'inserer un nouvel uilisateur dans la bd
	 * Et de mettre son token dans le fichier
	 * @param j
	 * @return
	 * @throws SQLException
	 */
	public boolean newUser(Joueur j) throws SQLException {
		if(g.setJoueur(j)) {
			new JoueurAuth(g.getJoueur(j.getEmail()));
			GestionToken token = new GestionToken();
			token.newToken();
			return true;
		} else return false; 
		
	}
	
	/**
	 * Permets de vérifier si un joueur existe dans la base de données
	 * @param j : Joueur comprenant un email et un pseudo 
	 * @return boolean : true si oui; false sinon
	 * @throws SQLException
	 */
	public boolean verifExistByEmail(Joueur j) throws SQLException {
		return new VerifExist(j).verifEmail();
	}
	
	/**
	 * Permets de vérifier si un joueur existe dans la base de données
	 * @param j : Joueur comprenant un email et un pseudo 
	 * @return boolean : true si oui; false sinon
	 * @throws SQLException
	 */
	public boolean verifExistByPseudo(Joueur j) throws SQLException {
		return new VerifExist(j).verifPseudo();
	}
	
	/**
	 * Permets de Hasher un string fourni en paramètre
	 * @param psd : String : qui sera hashé en SHA-256
	 * @return String : hashé 
	 */
	public String hashPassword(String psd) {
		Hashage hash = new Hashage(psd);
		String psdHash =""; 
		try {
			psdHash = hash.hashString();
		} catch (NoSuchAlgorithmException e) {

			e.printStackTrace();
		}
		return psdHash;
	}
	
	/**
	 * Permets de deconnecter un user et supprimer dans le fichier token son token
	 * @return boolean : true si cela a fonctionné; false sinon
	 */
	public boolean clearJoueurAuth() {
		JoueurAuth ja = new JoueurAuth(); 
		if(isConnect()) {
			ja.clear();
			GestionToken gestionToken;
			try {
				gestionToken = new GestionToken();
				gestionToken.clear();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			return true;
		} else return false;
	}
	
	
	/**
	 * Permets de connaitre le nom du joueur authentifié 
	 * @return String : nom du joueur
	 */
	public String getNomJoueur() {
		JoueurAuth ja = new JoueurAuth(); 
		return ja.getJoueurAuth().getNomJoueur();
	}
	
	/**
	 * Permets l'authentification d'un user
	 * @param email : String
	 * @param password : String
	 * @return boolean : true l'user est bien authentifié; false sinon
	 * @throws SQLException
	 * @throws NoSuchAlgorithmException
	 */
	public boolean loginJoueur(String email, String password) throws SQLException, NoSuchAlgorithmException {
		LoginJoueur lu = new LoginJoueur(email,password);
		if(lu.verifJoueur()) return true;
		else return false;
	}
	
	/**
	 * Ferme la connexion ouverte dans gestionRqt 
	 * @throws IOException 
	 */
	public void close() throws IOException {
		g.closeGestionRqt();
		co.exit();
		
	}
	
	public void send(int n) {
		try {
			co.sendMessage(n);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
