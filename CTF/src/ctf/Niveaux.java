package ctf;

import java.util.ArrayList;

/**
 * Déclaration des niveaux de jeu
 */
public class Niveaux {

    static final int gameWidth = 22;
    static final int gameHeight = 22;

    private static ArrayList<Niveau> niveaux = new ArrayList<Niveau>(){/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

	{

		this.add(new Niveau(1, "#####################|"
				+ "# @       0         #|"
				+ "#                   #|"
				+ "#                   #|"
				+ "#                   #|"
				+ "#                   #|"
				+ "#                   #|"
				+ "#                   #|"
				+ "#                   #|"
				+ "#                   #|"
				+ "#                   #|"
				+ "#                   #|"
				+ "#                   #|"
				+ "#                   #|"
				+ "#                   #|"
				+ "#                   #|"
				+ "#                   #|"
				+ "#                   #|"
				+ "#         0       @ #|"
				+ "#####################"));
    	//this.add(new Niveau(1, "######  ##### |#    #  #   # |#    ####   # |#           # |#  #######  # |####   ### ###|       #     #|       #     #|       #     #|      ##     #|      #*0@   #|      ########"));
     }};

    static Niveau getNiveau(int i) {
        return niveaux.get(i);
    }

    static int nombreNiveau() {
        return niveaux.size();
    }
}
