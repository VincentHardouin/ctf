package ctf;

import java.io.IOException;
import java.sql.SQLException;

import java.util.Optional;

import connexion.GestionRqt;
import connexion.GestionToken;
import connexion.ServerObjet;
import ctf.elementjeu.AbstractElementJeu;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;

import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;

import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import server.Server;

/**
 * Initialisation de l'interface graphique et de la classe JeuSokoban
 */
public class Main extends Application {


	GestionRqt g;

	public static void main(String[] args) {
		launch(args);
	}


	public int dialogServerOrNot() {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Lancement CTF");
		alert.setHeaderText("Ce jeu est un jeu en ligne 1vs1");
		alert.setContentText("Souhaitez-vous vous connecter à un serveur ou héberger une partie ?");
		//alert.s
		ButtonType buttonTypeOne = new ButtonType("Se connecter");
		ButtonType buttonTypeTwo = new ButtonType("Héberger");
		ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);

		alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo, buttonTypeCancel);

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == buttonTypeOne){
			return 1;
		} else if (result.get() == buttonTypeTwo) {
			return 2;
		} else {
			return 0;
		}
	}

	public Optional<ServerObjet> dialogServerChoice() throws SQLException {
		Optional<ServerObjet> result;
		ObservableList<ServerObjet> listServer = FXCollections.observableArrayList(g.getServer());


		Dialog<ServerObjet>  dialog = new Dialog<>();
		dialog.setTitle("Choice server");
		dialog.setHeaderText("Choose your server");
		dialog.setContentText("Choose your server:");

		ComboBox<ServerObjet> cb = new ComboBox<>(listServer);

		StringConverter<ServerObjet> converter = new StringConverter<ServerObjet>() {
			@Override
			public String toString(ServerObjet s) {
				return s.getServerName() + " place disponible :" + s.getNbrPlaceDispo(); 
			}

			@Override
			public ServerObjet fromString(String string) {
				return null;
			}
		};

		cb.setItems(listServer);
		cb.setConverter(converter);

		cb.setPrefSize(200, 20);


		HBox hb = new HBox();
		hb.getChildren().add(cb);
		hb.setPadding(new Insets(20));

		dialog.getDialogPane().setContent(hb);

		ButtonType okBtn  = new ButtonType("Ok", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(okBtn, ButtonType.CANCEL);



		dialog.setResultConverter(dialogButton -> {
			if (dialogButton == okBtn) {
				return cb.getValue();
			} 
			return null;
		});

		// Traditional way to get the response value.
		result = dialog.showAndWait();
		return result;

	}

	public void init(Stage stage, String ipLocal, String ipExt) throws SQLException, IOException {

		Pane gamePane = new Pane();

		StackPane stackPane = new StackPane(gamePane);
		int w = Niveaux.gameWidth * AbstractElementJeu.width;
		int h = Niveaux.gameHeight * AbstractElementJeu.width;
		stackPane.setMinSize(w, h);
		stackPane.setMaxSize(w, h);

		Button actionButton = new Button("Se connecter");
		actionButton.setFocusTraversable(false);
		Label nameLabel = new Label("Test");
		nameLabel.setPadding(new Insets(5));
		HBox topPane1 = new HBox(); 
		topPane1.setPadding(new Insets(5));
		topPane1.getChildren().add(actionButton);
		topPane1.getChildren().add(nameLabel);


		VBox topPane2 = new VBox(); 
		topPane2.setPadding(new Insets(5));
		topPane2.setAlignment(Pos.CENTER);


		Label label = new Label("0 - 0");
		label.setFont(new Font("Cambria", 32));

		Label labelKill = new Label("0 - 0"); 
		labelKill.setFont(new Font("Cambria", 20));
		topPane2.getChildren().add(label);
		topPane2.getChildren().add(labelKill);

		Button classementBtn = new Button("Classement");
		classementBtn.setFocusTraversable(false);
		HBox bottomPane = new HBox(classementBtn);
		bottomPane.setPadding(new Insets(5));

		VBox vbox = new VBox();
		vbox.getChildren().add(topPane1);
		vbox.getChildren().add(topPane2);

		vbox.getChildren().add(stackPane);

		vbox.getChildren().add(bottomPane);

		Scene scene = new Scene(vbox);

		new JeuCtf(stage,gamePane, label,labelKill, actionButton, nameLabel, classementBtn,ipLocal, ipExt);

		stage.setScene(scene);
		stage.setTitle("CTF");
		stage.show();

	}


	public Optional<String> dialogSetServerName() {
		Optional<String> result ; 			
		TextInputDialog dialog = new TextInputDialog("servername");
		dialog.setTitle("Information about server");
		dialog.setHeaderText("Please fill the form");
		dialog.setContentText("Choose server name:");

		result = dialog.showAndWait();
		
		if(result.isPresent()) {
			return result; 
		} else return Optional.empty();
	}


	@Override
	public void start(Stage stage) throws SQLException, IOException {
		double debut = System.nanoTime(); 


		GestionToken gt = new GestionToken(); 
		gt.read();

		g = new GestionRqt(); 




		boolean cancel = false;

		do {
			cancel = false;
			int result = dialogServerOrNot();
			switch(result) {
			case 0: 
				stage.close();
				break;
			case 1: 
				// Choisir server
				Optional<ServerObjet> server = dialogServerChoice();
				if(server.isPresent()) {
					String ipExt = server.get().getIpExt(); 
					String ipLocal = server.get().getIpLocal();
					init(stage,ipLocal,ipExt);
				}
				break;
			case 2: 
				// Heberger 
				Optional<String> serverName = dialogSetServerName();
				if(!serverName.isPresent()) {
					cancel = true; 
				} else {
					Server s = new Server(serverName.get());
					Thread t = new Thread(s);
					t.start();
					String ipExt = s.getIpExt(); 
					String ipLocal = s.getIpLocal();
					init(stage,ipLocal,ipExt);
				}
				break; 
			}
			
		} while(cancel);


		double fin = System.nanoTime(); 

		System.out.println("Temps main : " + (fin-debut)/1e9 + "sec" );

	}

}
