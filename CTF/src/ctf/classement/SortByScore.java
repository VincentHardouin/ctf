package ctf.classement;

import java.util.Comparator;

import ctf.elementjeu.Joueur;


/** 
 * Permets de trier par score les joueurs
 * Utilisé car Comparable est utilisé pour AbstractElementJeu 
 * et il est possible d'avoir q'un seul comparable 
 *
 */
public class SortByScore  implements Comparator<Joueur> {

	@Override
	public int compare(Joueur o1, Joueur o2) {
		return o1.getScore() - o2.getScore();
	}

}
