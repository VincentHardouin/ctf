package ctf.action;

import ctf.Coord;
import ctf.TerrainCtf;

/**
 * Enregistre les informations nécessaires pour un déplacement du joueur,
 * sans déplacement ni élimination de pierre
 */
public class CaptureFlag extends AbstractAction {

    public CaptureFlag(Coord coordDepart, Coord coordCible) {
        super(coordDepart, coordCible);
        System.out.println("Depart : " + coordDepart.toString() + " Cible: " +coordCible.toString());
    }

    /**
     * Execute ce déplacement sur un objet TerrainSokoban
     */
    public void execute(TerrainCtf t) {
    	t.cacheFlagAdverse();
    	t.deplaceJoueur(coordCible);
        t.setJoueurPorteur(true);
    }
    
    public void executeAdverse(TerrainCtf t) {
    	t.cacheFlag();
    	t.deplaceJoueurAdverse(coordCible);
        t.setJoueurAdversePorteur(true);
    }
    

    /**
     * Annule ce déplacement sur un objet TerrainSokoban
     */
    public void executeDefait(TerrainCtf t) {
        t.deplaceJoueur(coordDepart);
    }

}
