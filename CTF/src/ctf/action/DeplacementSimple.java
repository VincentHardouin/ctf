package ctf.action;

import ctf.Coord;
import ctf.TerrainCtf;

/**
 * Enregistre les informations nécessaires pour un déplacement du joueur,
 * sans déplacement ni élimination de pierre
 */
public class DeplacementSimple extends AbstractAction {

    public DeplacementSimple(Coord coordDepart, Coord coordCible) {
        super(coordDepart, coordCible);
    }

    /**
     * Execute ce déplacement sur un objet TerrainSokoban
     */
    public void execute(TerrainCtf t) {
        t.deplaceJoueur(coordCible);
    }
    
    public void executeAdverse(TerrainCtf t) {
        t.deplaceJoueurAdverse(coordCible);
    }

    /**
     * Annule ce déplacement sur un objet TerrainSokoban
     */
    public void executeDefait(TerrainCtf t) {
        t.deplaceJoueur(coordDepart);
    }

}
