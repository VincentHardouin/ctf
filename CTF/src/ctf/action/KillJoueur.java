package ctf.action;

import ctf.Coord;
import ctf.JeuCtf;
import ctf.TerrainCtf;
import javafx.application.Platform;

public class KillJoueur implements Runnable {
	public KillJoueur() {
		System.out.println("Kill");
	}

	@Override
	public void run() {
		Coord coordMort = TerrainCtf.getJoueurStatic().getCoord();
		TerrainCtf.setCoordJoueur(null);
		if(TerrainCtf.getJoueurStatic().isPorteur()) {
			TerrainCtf.getJoueurStatic().setPorteur(false);
			TerrainCtf.afficheFlagAdverseStatic(coordMort);
		}
			
		Platform.runLater(new Runnable(){
			
			@Override
			public void run() {	
				JeuCtf.addKillJoueurAdverse();
			}
			
		});
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		TerrainCtf.setCoordJoueur(new Coord(18,18));
		Thread.interrupted();
		
	}
	
}
