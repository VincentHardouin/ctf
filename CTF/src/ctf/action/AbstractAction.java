package ctf.action;

import ctf.Coord;
import ctf.TerrainCtf;

/**
 * Base des différentes actions de jeu
 */
public abstract class AbstractAction {

    abstract public void execute(TerrainCtf t);
    abstract public void executeAdverse(TerrainCtf t);
    abstract public void executeDefait(TerrainCtf t);

    Coord coordDepart;
    Coord coordCible;

    public AbstractAction(Coord coordDepart, Coord coordCible) {
        this.coordDepart = coordDepart;
        this.coordCible = coordCible;
    }
	
}
