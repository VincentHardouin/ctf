package ctf.action;

import ctf.Coord;
import ctf.JeuCtf;
import ctf.TerrainCtf;
import javafx.application.Platform;

public class KillJoueurAdverse implements Runnable {
	public KillJoueurAdverse() {
		System.out.println("Kill");
	}

	@Override
	public void run() {
		Coord coordMort = TerrainCtf.getJoueurAdverseStatic().getCoord();
		TerrainCtf.setCoordJoueurAdverse(null);
		if(TerrainCtf.getJoueurAdverseStatic().isPorteur()) {
			TerrainCtf.getJoueurAdverseStatic().setPorteur(false);
			TerrainCtf.afficheFlagStatic(coordMort);
		}
		Platform.runLater(new Runnable(){
			
			@Override
			public void run() {
				JeuCtf.addKillJoueur();		
			}
			
		});
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		TerrainCtf.setCoordJoueurAdverse(new Coord(2,1));
		Thread.interrupted();
		
	}
	
}
