package ctf.action;

import ctf.Coord;
import ctf.JeuCtf;
import ctf.TerrainCtf;

public class ReturnFlagToBase extends AbstractAction {
	
	private final Coord EMPLACEMENT_FLAG_ADVERSE = new Coord(10,1);
	private final Coord EMPLACEMENT_FLAG = new Coord(10,18);
	
	public ReturnFlagToBase(Coord coordDepart, Coord coordCible) {
		super(coordDepart, coordCible);
	}

	@Override
	public void execute(TerrainCtf t) {
		t.afficheFlag(EMPLACEMENT_FLAG);
    	t.deplaceJoueur(coordCible);
        t.setJoueurPorteur(false);		
	}

	@Override
	public void executeAdverse(TerrainCtf t) {
		t.afficheFlagAdverse(EMPLACEMENT_FLAG_ADVERSE);
    	t.deplaceJoueurAdverse(coordCible);
        t.setJoueurAdversePorteur(false);
	}

	@Override
	public void executeDefait(TerrainCtf t) {
		
	}

}
