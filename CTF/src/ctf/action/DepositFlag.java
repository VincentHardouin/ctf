package ctf.action;

import ctf.Coord;
import ctf.JeuCtf;
import ctf.TerrainCtf;
import javafx.application.Platform;

public class DepositFlag extends AbstractAction {
	
	
	private final Coord EMPLACEMENT_FLAG_ADVERSE = new Coord(10,1);
	private final Coord EMPLACEMENT_FLAG = new Coord(10,18);
	
    public DepositFlag(Coord coordDepart, Coord coordCible) {
        super(coordDepart, coordCible);
        System.out.println("Depart : " + coordDepart.toString() + " Cible: " +coordCible.toString());
    }

    /**
     * Execute ce déplacement sur un objet TerrainSokoban
     */
    public void execute(TerrainCtf t) {
    	t.afficheFlagAdverse(EMPLACEMENT_FLAG_ADVERSE);
    	t.deplaceJoueur(coordCible);
        t.setJoueurPorteur(false);
        JeuCtf.addPointJoueur();
    }
    
    public void executeAdverse(TerrainCtf t) {
    	t.afficheFlag(EMPLACEMENT_FLAG);
    	t.deplaceJoueurAdverse(coordCible);
        t.setJoueurAdversePorteur(false);
        Platform.runLater(new Runnable(){
			@Override
			public void run() {	
				JeuCtf.addPointJoueurAdverse();
			}
			
		});
    }

    /**
     * Annule ce déplacement sur un objet TerrainSokoban
     */
    public void executeDefait(TerrainCtf t) {
        t.deplaceJoueur(coordDepart);
    }

}