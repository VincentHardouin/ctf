package ctf.action;

import ctf.Coord;
import ctf.TerrainCtf;

/**
 * Enregistre les informations nécessaires pour un déplacement du joueur
 * poussant une pierre (qui n'est pas envoyée dans une sortie)
 */
public class DeplacementPierre extends AbstractAction {

    Coord coordCiblePierre;

    public DeplacementPierre(Coord coordDepart, Coord coordCible, Coord coordCiblePierre) {
        super(coordDepart, coordCible);
        this.coordCiblePierre = coordCiblePierre;
    }

    public void execute(TerrainCtf t) {
      
        t.deplaceJoueur(coordCible);
    }
    
    public void executeAdverse(TerrainCtf t) {
        
        t.deplaceJoueurAdverse(coordCible);
    }

    public void executeDefait(TerrainCtf t) {
        t.deplaceJoueur(coordDepart);
    
    }
}
