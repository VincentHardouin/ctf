package ctf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import ctf.action.AbstractAction;
import ctf.action.CaptureFlag;
import ctf.action.DeplacementSimple;
import ctf.action.DepositFlag;
import ctf.action.KillJoueurAdverse;
import ctf.action.ReturnFlagToBase;
import ctf.elementjeu.AbstractElementJeu;
import ctf.elementjeu.Balle;
import ctf.elementjeu.Flag;
import ctf.elementjeu.Joueur;
import ctf.elementjeu.Mur;
import ctf.elementjeu.Pierre;
import ctf.elementjeu.Sortie;
import javafx.scene.layout.Pane;

/**
 * Gère le plateau de jeu
 */
public class TerrainCtf {

	private static Pane gamePane;

	private static Joueur joueur;
	private static Joueur joueurAdverse;
	private static Flag flag;
	private static Flag flagAdverse;

	private static List<Mur> murs;

	private static List<Flag> flags;
	
	
	private final Coord BASE = new Coord(10,18);
	private final static Coord BASE_ADVERSE = new Coord(10,1);
	

	/**
	 * Enregistre une référence vers le Pane devant afficher le terrain
	 */
	public TerrainCtf(Pane gamePane) {
		double debut = System.nanoTime(); 

		TerrainCtf.gamePane = gamePane;
		double fin = System.nanoTime(); 
		System.out.println("Temps constructeur TerrainCtf : " + (fin-debut)/1e9 + "sec" );
	}

	/**
	 * Prépare le terrain d'après un niveau de jeu
	 */
	public void initNiveau(String niveau) {

		gamePane.getChildren().clear();
		murs = new ArrayList<>();
		flags = new ArrayList<>();
		int x = 0;
		int y = 0;

		int j = 0;
		int f = 0;

		for(char c : niveau.toCharArray()) {
			switch (c) {
			case '|' :
				y++;
				x=0;
				break;
			case '@':
				if(j == 0) {
					joueurAdverse = new Joueur(x,y, gamePane);
					j++;
				} else {
					joueur = new Joueur(x, y, gamePane);
				}
				x++;
				break;
			case '#':
				murs.add(new Mur(x, y, gamePane));
				x++;
				break;
			case '0':
				if(f == 0) {
					flagAdverse = new Flag(x,y, gamePane); 
					flags.add(flagAdverse); 
					f++;
				} else {

					flag = new Flag(x, y, gamePane);
					flags.add(flag); 
				}
				x++;
				break;
			default:
				x++;
			}
		}

		// définition des dimensions du gamePane
		int w = x * AbstractElementJeu.width;
		int h = (y + 1) * AbstractElementJeu.width;
		gamePane.setMinSize(w, h);
		gamePane.setMaxSize(w, h);

	}

	/**
	 * Retourne le cas échéant un objet Action correspondant au déplacement possible
	 * selon le paramètre depl et l'état du plateau de jeu
	 */
	Optional<AbstractAction> prepareDeplacement(Coord depl) {

		// carreau souhaité
		if(joueur.getCoord() != null) {
			
			Coord coordJoueur = joueur.getCoord();
			Coord coordCible = coordJoueur.add(depl);
			Optional<AbstractElementJeu> cible = contenu(coordCible);
			
			if (!cible.isPresent()) {
				return Optional.of(new DeplacementSimple(coordJoueur, coordCible));
			} else {
				if (cible.get() instanceof Flag) {
					if(coordCible.compareTo(flagAdverse.getCoord()) == 0) {
						return Optional.of(new CaptureFlag(coordJoueur, coordCible));
					} else if(coordCible.compareTo(flag.getCoord()) == 0) {
						if(coordCible.compareTo(BASE) == 0) {
							if(joueur.isPorteur()) {
								return Optional.of(new DepositFlag(coordJoueur, coordCible));
							} else return Optional.of(new DeplacementSimple(coordJoueur, coordCible));							
						} else return Optional.of(new ReturnFlagToBase(coordJoueur, coordCible));
					}
				}
			}
		}
		return Optional.empty();
	}

	static Optional<AbstractAction> prepareDeplacementAdverse(Coord depl) {

		// carreau souhaité
		Coord coordJoueur = joueurAdverse.getCoord();
		Coord coordCible = coordJoueur.add(depl);
		Optional<AbstractElementJeu> cible = contenu(coordCible);

		if (!cible.isPresent()) {
			// déplace le joueur
			return Optional.of(new DeplacementSimple(coordJoueur, coordCible));
		} else {
			if (cible.get() instanceof Flag) {

				if(coordCible.compareTo(flag.getCoord()) == 0) {

					return Optional.of(new CaptureFlag(coordJoueur, coordCible));
				} else if(coordCible.compareTo(flagAdverse.getCoord()) == 0) {
					if(coordCible.compareTo(BASE_ADVERSE) == 0) {
						if(joueurAdverse.isPorteur()) {
							return Optional.of(new DepositFlag(coordJoueur, coordCible));
						} else return Optional.of(new DeplacementSimple(coordJoueur, coordCible));							
					} else return Optional.of(new ReturnFlagToBase(coordJoueur, coordCible));
				
				}


			}
		}
		return Optional.empty();
	}

	void verifTrajectoire(List<Coord> list) {
		for(Coord c: list) {
			contenu(c);
		}
	}

	/**
	 * Recherche si un élément du jeu (autre que le joueur) se trouve à l'emplacement spécifié par les coordonnées
	 */
	static Optional<AbstractElementJeu> contenu(Coord coord) {
		for(Mur x: murs) {
			if (x.hasCoord(coord)) return Optional.of(x);
		}
		for(Flag x: flags) {
			if (x.hasCoord(coord)) return Optional.of(x);
		}
		return Optional.empty();
	}


	Optional<AbstractElementJeu> contenuTrajectoire(List<Coord> list) {
		for(Coord c: list) {
			for(Mur x: murs) {
				if (x.hasCoord(c)) return Optional.of(x);
			}
			if(joueurAdverse.hasCoord(c)) return Optional.of(joueurAdverse);			
		}

		return Optional.empty();
	}

	static Optional<AbstractElementJeu> contenuTrajectoireForJoueurAdverse(List<Coord> list) {
		for(Coord c: list) {
			for(Mur x: murs) {
				if (x.hasCoord(c)) return Optional.of(x);
			}
			if(joueur.hasCoord(c)) return Optional.of(joueur);			
		}

		return Optional.empty();
	}



	public void deplaceJoueur(Coord cible) {
		joueur.setCoord(cible);
	}

	public void deplaceJoueurAdverse(Coord cible) {
		joueurAdverse.setCoord(cible);
	}


	/**
	 * Permets de convertir le terrain en cours en chaine de caractère
	 * 
	 * @return Sttring : le terrain actuel en chaine de caractère
	 */
	public String getNiveau() {
		List<AbstractElementJeu> abstractElements = getElementsNiveau();

		int maxX = getMaxX(abstractElements); 
		int maxY = getMaxY(abstractElements); 

		int indexAbstractElmts = 0; 

		int longueurTotal = (maxY*maxX) + maxY-1; 

		StringBuilder builder = new StringBuilder(longueurTotal);
		for(int indexY=0; indexY<=maxY; indexY++) {
			for(int indexX=0; indexX<=maxX; indexX++) {
				Coord c = abstractElements.get(indexAbstractElmts).getCoord();
				if(c.getX() == indexX && c.getY() == indexY) {
					AbstractElementJeu a = abstractElements.get(indexAbstractElmts);
					if(a instanceof Mur) builder.append('#'); 
					else if(a instanceof Pierre) builder.append('0'); 
					else if(a instanceof Joueur) builder.append('@'); 
					else if(a instanceof Sortie) builder.append('*'); 
					indexAbstractElmts++; 
				} else builder.append(' ');
			}
			if(indexY != maxY) builder.append('|'); 
		}

		System.out.println(builder.toString());
		return builder.toString();      
	}

	/**
	 * Permets d'obtenir tous les élements du niveau trié par coordonnées 
	 * @return List<AbstractElementJeu> : list de tous les élements du niveau trié
	 */
	private List<AbstractElementJeu> getElementsNiveau() {
		List<AbstractElementJeu> abstractElements = new ArrayList<AbstractElementJeu>(); 
		for(Flag x: flags) {
			abstractElements.add(x); 
		}
		for(Mur x: murs) {
			abstractElements.add(x); 
		}

		abstractElements.add(joueur);
		Collections.sort(abstractElements);
		return abstractElements;
	}

	/**
	 * Permets de connaitre le x maximum dans une liste d'élement de Jeu 
	 * @param list List<AbstractElementJeu> : list d'élement de jeu 
	 * @return int : le x maximum
	 */
	public int getMaxX(List<AbstractElementJeu> list) {
		int x = 0; 
		for(AbstractElementJeu a : list) {
			if(a.getCoord().getX() > x) x = a.getCoord().getX(); 
		}
		return x;
	}

	/**
	 * Permets de connaitre le y maximum dans une liste d'élement de Jeu 
	 * @param list List<AbstractElementJeu> : list d'élement de jeu 
	 * @return int : le y maximum
	 */
	public int getMaxY(List<AbstractElementJeu> list) {
		int y = 0; 
		for(AbstractElementJeu a : list) {
			if(a.getCoord().getY() > y) y = a.getCoord().getY(); 
		}
		return y;
	}


	public void tir(int direction) {
		Coord dep = joueur.getCoord();
		Coord finale = null; 
		List<Coord> verifList = new ArrayList<>();
		switch(direction) {
		case 1: 
			for(int i=(-1);i>(-7);i--) {
				verifList.add(dep.add(new Coord(0, i))); 
			}
			break; 
		case 2:
			for(int i=1;i<7;i++) {
				verifList.add(dep.add(new Coord(i, 0))); 
			}
			break; 
		case 3: 
			for(int i=1;i<=7;i++) {
				verifList.add(dep.add(new Coord(0, i))); 
			}
			break; 
		case 4: 
			for(int i=(-1);i>(-7);i--) {
				verifList.add(dep.add(new Coord(i, 0))); 
			}
			break;
		}


		boolean kill = false;
		int index = verifList.size() - 1;
		// Verif toutes les coord de la liste puis attribuer les dernieres coord
		Optional<AbstractElementJeu> option = contenuTrajectoire(verifList);
		if (option.isPresent()) {
			AbstractElementJeu a = option.get();
			if(a instanceof Mur) {
				index = indexOfElement(a, verifList) - 1;
			} else if(a instanceof Joueur) {
				index = indexOfElement(a, verifList) - 1;
				System.out.println("Kill joueur adverse");
				kill = true;
			}

		}
		if(index != -1 ) {
			finale = verifList.get(index);
			Balle b = new Balle(verifList.get(0),direction,index+1,false,kill,gamePane);
			b.setCoord(finale);
		}


	}

	public static void tirJoueurAdverse(int direction) {
		Coord dep = joueurAdverse.getCoord();
		Coord finale = null; 
		List<Coord> verifList = new ArrayList<>();
		direction = direction -10;
		switch(direction) {
		case 1: 

			for(int i=1;i<=7;i++) {
				verifList.add(dep.add(new Coord(0, i))); 
			}
			break; 
		case 2:
			for(int i=(-1);i>(-7);i--) {
				verifList.add(dep.add(new Coord(i, 0))); 
			}
			break; 
		case 3: 
			for(int i=(-1);i>(-7);i--) {
				verifList.add(dep.add(new Coord(0, i))); 
			}
			break; 
		case 4: 
			for(int i=1;i<7;i++) {
				verifList.add(dep.add(new Coord(i, 0))); 
			}
			break;
		}

		boolean kill = false;

		int index = verifList.size() - 1;
		// Verif toutes les coord de la liste puis attribuer les dernieres coord
		Optional<AbstractElementJeu> option = contenuTrajectoireForJoueurAdverse(verifList);
		if (option.isPresent()) {
			AbstractElementJeu a = option.get();
			if(a instanceof Mur) {
				index = indexOfElement(a, verifList) - 1;
			} else if(a instanceof Joueur) {
				index = indexOfElement(a, verifList) - 1;
				System.out.println("Kill Joueur");
				kill = true;
			}

		}
		if(index != -1 ) {
			finale = verifList.get(index);
			Balle b = new Balle(verifList.get(0),direction,index+1,kill,false,gamePane);
			b.setCoord(finale);
		}

	}

	static int indexOfElement(AbstractElementJeu a, List<Coord> list) {
		int index = 0;
		for(int i=0; i<list.size(); i++) {
			if(list.get(i).compareTo(a.getCoord()) == 0) index = i;
		}
		return index;
	}


	public static void moveJoueurAdverse(int n) {
		Coord depl = null;
		switch(n) {
		case 1:
			depl = new Coord(0,1);
			break; 
		case 2:
			depl = new Coord(-1,0);
			break; 
		case 3:
			depl = new Coord(0,-1);
			break; 
		case 4:
			depl = new Coord(1,0);
			break; 
		}

		prepareDeplAdverse(depl);
	}

	public static void prepareDeplAdverse(Coord depl) {
		Optional<AbstractAction> optionCommande = prepareDeplacementAdverse(depl);

		if (optionCommande.isPresent()) {
			AbstractAction c = optionCommande.get();

			c.executeAdverse(new TerrainCtf(gamePane));
			if (c instanceof DeplacementSimple) {


			} else if(c instanceof CaptureFlag) {

			}

		}

	}


	public void setJoueurPorteur(boolean bool) {
		joueur.setPorteur(bool);
	}

	public void setJoueurAdversePorteur(boolean bool) {
		joueurAdverse.setPorteur(bool);
	}

	public Joueur getJoueurAdverse() {
		return joueurAdverse;
	}
	
	public static Joueur getJoueurAdverseStatic() {
		return joueurAdverse; 
	}
	
	public static Joueur getJoueurStatic() {
		return joueur; 
	}
	
	
	public static void setCoordJoueurAdverse(Coord c) {
		joueurAdverse.setCoord(c);
	}
	
	public static void setCoordJoueur(Coord c) {
		joueur.setCoord(c);
	}
	
	public void cacheFlagAdverse() {
		flagAdverse.setVisible(false);
	}

	public void cacheFlag() {
		flag.setVisible(false);
	}

	public void afficheFlagAdverse(Coord coord) {
		flagAdverse.setCoord(coord);
	}

	public void afficheFlag(Coord coord) {
		flag.setCoord(coord);
	}
	
	public static void afficheFlagAdverseStatic(Coord coord) {
		flagAdverse.setCoord(coord);
	}

	public static void afficheFlagStatic(Coord coord) {
		flag.setCoord(coord);
	}
	
	

}
