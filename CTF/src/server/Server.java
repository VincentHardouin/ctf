package server;

import connexion.GestionRqt;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;

import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Server implements Runnable {

	private final static int PORT = 5656; 
	static List<DataOutputStream> clients = new ArrayList<>(); 
	
	private static int idServer;
	
	private final static int NBR_PLACE = 2;
	private static GestionRqt g;
	
	private String ipLocal;
	private String ipExt;
	private String serverName; 
	
	public Server(String serverName) throws SQLException, IOException {
		this.serverName = serverName;
		
		g = new GestionRqt(); 
		
		InetAddress inetAddress = InetAddress.getLocalHost(); 
		
		 
		ipLocal = inetAddress.getHostAddress();
		System.out.println("Serveur ! IP : " + ipLocal);
		
		
		URL whatismyip = new URL("http://checkip.amazonaws.com");
		BufferedReader in = new BufferedReader(new InputStreamReader(
				whatismyip.openStream()));
		
		ipExt = in.readLine(); //you get the IP as a String
		System.out.println("External ip " + ipExt);
		
	}

	public String getIpLocal() {
		return ipLocal;
	}

	public String getIpExt() {
		return ipExt;
	}

	@Override
	public void run() {


		try(ServerSocket ss = new ServerSocket(PORT,2)) {
			
		
			idServer = g.setServer(ipLocal,ipExt,serverName);
			
			ss.setSoTimeout(30000);
			
			System.out.println("En attente d'un client");
			
			
			while(clients.size() < NBR_PLACE) {
				Socket s = ss.accept();
				DataInputStream dIn = new DataInputStream(s.getInputStream());
				DataOutputStream dOut = new DataOutputStream(s.getOutputStream());
				clients.add(dOut);
				
				ClientHandler client = new ClientHandler(s, dIn); 
				Thread t = new Thread(client);
				g.setNbrPlaceDisponible((NBR_PLACE-clients.size()), idServer);
				t.start();
			}
			
			
			ss.close();
			
			
		} catch (IOException | SQLException e) {
			
			e.printStackTrace();
		} finally {
			g.deleteServer(idServer);
			g.closeGestionRqt();
		}
		
		
	}


}

class ClientHandler implements Runnable {
	private Socket socket;
	private DataInputStream dIn; 
	private static int connexion = 0;
	private int id;
	private boolean running;
	private boolean deco; 
	public ClientHandler(Socket socket, DataInputStream dIn) {
		running = true;
		deco = false;
		this.socket = socket;
		this.dIn = dIn; 
		id = connexion;
		connexion++;
		System.out.println("Client n:" + id + " ip : " + socket.getInetAddress());
	}
	
	public void terminate() {
		running = false;
	}
	
	@Override
	public void run() {
		try {
			int mouvement;
			while(running) {
				if(!socket.isClosed()) {
					
					mouvement = dIn.readInt();
					if(mouvement == 999) deco = true;
					System.out.println("Transmission : msg de "+ id + " : "+mouvement);
					for(int i=0;i < Server.clients.size(); i++) { 
						if(i != id) {
							DataOutputStream dos = Server.clients.get(i);
							try {
								dos.writeInt(mouvement);
								dos.flush();
								System.out.println("Msg transmit à id : " + i);
							}
							catch (Exception e) { }
						} else if(i == id && deco) {
							DataOutputStream dos = Server.clients.get(i);
							dos.close();
							dIn.close();
							Server.clients.remove(i);
							System.out.println("Deconnexion de l'id : "+  i);
						}
					}
					if(Server.clients.size() == 0) running = false;
				} 
			}

		}
		catch (Exception e) { 
			// e.printStackTrace();
		}
		
		System.out.println("server end");
	
		
	}
}