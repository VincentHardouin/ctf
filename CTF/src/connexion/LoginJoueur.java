package connexion;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import ctf.elementjeu.Joueur;

/**
 * Permets de vérifier le mot de passe d'un user lors de la connexion
 * 
 * @author vincenthardouin
 *
 */
public class LoginJoueur {

	private GestionRqt g = new GestionRqt();
	private String text;
	private String password;


	/**
	 * Constructeur nécessitant l'email et le mot de passe (non hashé) de l'user
	 * que l'on souhaite connecté à l'application
	 * 
	 * @param text    : String
	 * @param password : String : pas hashé
	 * @throws SQLException
	 */
	public LoginJoueur(String text, String password) throws SQLException {
		g = new GestionRqt();
		this.text = text;
		this.password = password;
	}

	/**
	 * Vérifie si le joueur existe dans la base de données, si oui le récupère On
	 * hash le password passé dans le constructeur de la class et on le vérifie
	 * avec celui de la base de données S'ils sont identiques on identifie l'user
	 * en instanciant le singleton userAuth
	 * 
	 * @return boolean : true si cela a fonctionné; false sinon
	 * @throws SQLException
	 * @throws NoSuchAlgorithmException
	 */
	public boolean verifJoueur() throws SQLException, NoSuchAlgorithmException {

		VerifExist verif = new VerifExist(text); 
	
		if (verif.verifEmail()) {
			Joueur joueurBD = g.getJoueur(text);
			Hashage hash = new Hashage(password);
			String passwordSaisieHash = hash.hashString();
			String passwordBD = joueurBD.getPassword();
			

			// Test mdp
			if (passwordSaisieHash.compareTo(passwordBD) == 0) {
				new JoueurAuth(joueurBD);
				GestionToken token = new GestionToken(); 
				token.newToken();
				
				return true;
			} else
				return false;
		} else if (verif.verifPseudo()) {
			Joueur joueurBD = g.getJoueurByPseudo(text);
			Hashage hash = new Hashage(password);
			String passwordSaisieHash = hash.hashString();
			String passwordBD = joueurBD.getPassword();
			

			// Test mdp
			if (passwordSaisieHash.compareTo(passwordBD) == 0) {
				new JoueurAuth(joueurBD);
				GestionToken token = new GestionToken(); 
				token.newToken();
				
				return true;
			} else
				return false;
		}
		return false;
	}
}
