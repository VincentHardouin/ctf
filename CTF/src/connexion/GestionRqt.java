package connexion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import ctf.Niveau;
import ctf.classement.SortByScore;
import ctf.elementjeu.Joueur;

public class GestionRqt extends Connect {

	private Connection connexion;
	
	
	/**
	 * Constructeur permettant de récupérer la connexion à la base de données
	 * @throws SQLException
	 */
	public GestionRqt() throws SQLException {
		try {
			this.connexion = Connect.getConnexion();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
	}
	
	private ServerObjet rsetToServer(final ResultSet rset) throws SQLException {
		ServerObjet s = new ServerObjet();
		s.setIpLocal(rset.getString("ipLocal"));
		s.setIpExt(rset.getString("ipExt"));
		s.setServerName(rset.getString("serverName"));
		s.setNbrPlaceDispo(rset.getInt("placeDispo"));
		return s;
	}
	
	public List<ServerObjet> getServer() throws SQLException {
		Statement s = null;
		ResultSet resultat = null;
		String query = "SELECT * FROM server WHERE placeDispo > 0"; 
		ServerObjet server = null; 
		List<ServerObjet> list = new ArrayList<>();
		try {
			s = connexion.createStatement();
	
			resultat = s.executeQuery(query);
			while (resultat.next()) {
				server = rsetToServer(resultat);
				list.add(server);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeRqt(resultat,s);
		}
		return list;
	}
	
	
	/**
	 * Permets de retourner un joueur pour un tuple de résultat de requête
	 * @param rset
	 * @return Joueur
	 * @throws SQLException
	 */
	private Joueur rsetToJoueur(final ResultSet rset) throws SQLException {
		Joueur u = new Joueur();
		u.setIdJoueur(rset.getInt("idJoueur"));
		u.setNomJoueur(rset.getString("nom"));
		u.setEmail(rset.getString("email"));
		u.setPassword(rset.getString("password"));
		u.setToken(rset.getString("token"));
		return u;
	}

	/**
	 * Permets de récupérer un joueur à partir de son adresse mail
	 * @param email
	 * @return Joueur
	 * @throws SQLException
	 */
	public Joueur getJoueur(String email) throws SQLException {
		PreparedStatement ps = null;
		ResultSet resultat = null;
		Joueur joueur = null;
		String query = "SELECT * FROM joueur WHERE email = ?";
		try {
			ps = connexion.prepareStatement(query);
			ps.setString(1, email);
			resultat = ps.executeQuery();
			while (resultat.next()) {
				joueur = rsetToJoueur(resultat);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ps.close();
			resultat.close();
		}
		return joueur;
	}
	
	
	/**
	 * Permets de récupérer un joueur à partir de son pseudo
	 * @param pseudo
	 * @return Joueur
	 * @throws SQLException
	 */
	public Joueur getJoueurByPseudo(String pseudo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet resultat = null;
		Joueur joueur = null;
		String query = "SELECT * FROM joueur WHERE nom = ?";
		try {
			ps = connexion.prepareStatement(query);
			ps.setString(1, pseudo);
			resultat = ps.executeQuery();
			while (resultat.next()) {
				joueur = rsetToJoueur(resultat);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ps.close();
			resultat.close();
		}
		return joueur;
	}
	
	/**
	 * Permet d'obtenir le joueur à partir de son id
	 * @param idJoueur : int 
	 * @return Joueur : Joueur que l'on a récupéré 
	 */
	public Joueur getJoueur(int idJoueur) {
		PreparedStatement ps = null;
		ResultSet resultat = null;
		String query = "SELECT * FROM joueur WHERE idJoueur = ?"; 
		Joueur j = new Joueur();
		try {
			ps = connexion.prepareStatement(query);
			ps.setInt(1, idJoueur);
			resultat = ps.executeQuery();
			while (resultat.next()) {
				j = rsetToJoueur(resultat);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeRqt(resultat, ps);
		}
		return j;
	}

	/**
	 * Permet de retourner un Joueur nom et score pour un niveau
	 * @param rset
	 * @return Joueur
	 * @throws SQLException
	 */
	private Joueur rsetToJoueurScore(final ResultSet rset) throws SQLException {
		Joueur u = new Joueur();
		u.setIdJoueur(rset.getInt("idJoueur"));
		u.setNomJoueur(rset.getString("nom"));
		u.setScore(rset.getInt("score"));
		return u;
	}


	/**
	 * Permet d'enregistrer  un joueur dans la base de données 
	 * Retourne vrai si cela a fonctionné 
	 * @param joueur : Joueur que l'on souhaite insérer 
	 * @return Boolean : True cela a fonctionné, sinon false
	 */
	public Boolean setJoueur(Joueur joueur) {
		PreparedStatement ps = null;
		String query = "INSERT INTO joueur (nom,email,password)" + "VALUES(?,?,?)";

		try {
			ps = connexion.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, joueur.getNomJoueur());
			ps.setString(2, joueur.getEmail());
			ps.setString(3, joueur.getPassword());

			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		System.out.println("Joueur bien ajouté");
		return true;

	}
	
	/**
	 * Permets de retourner la liste des joueurs pour un niveau trié dans l'ordre croissante par score
	 * @param niveau : niveau dont on souhaite le classement
	 * @return List<Joueur> : list de joueur trié par score; vide s'il n'y a pas de score stocké pour un niveau
	 */
	public List<Joueur> getJoueursScoreByLevel(int niveau) {
		PreparedStatement ps = null;
		ResultSet resultat = null;
		List<Joueur> listeJoueur = null;
		Joueur joueur = null;
		String query = "SELECT j.idJoueur, nom, score FROM score sc\r\n" + 
				"INNER JOIN joueur j ON j.idJoueur = sc.idJoueur \r\n" + 
				"WHERE sc.niveau = ? AND sc.idScore NOT IN(SELECT idScore FROM sauvegarde)";
		try {
			ps = connexion.prepareStatement(query);
			ps.setInt(1, niveau);
			listeJoueur = new ArrayList<Joueur>();
			resultat = ps.executeQuery();
			while(resultat.next()) {
				joueur = rsetToJoueurScore(resultat);
				listeJoueur.add(joueur);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeRqt(resultat, ps);
		}

		Collections.sort(listeJoueur, new SortByScore());
		return listeJoueur;
	}
	
	
	/**
	 * Permets d'obtenir le score enregistré dans la base de donnée pour un niveau du joueur connecté 
	 * Retourne un score de 0 s'il n'y pas de score stocké dans la base de données
	 * 
	 * @param niveau : int : niveau dont on cherche le score 
	 * @return Joueur : contenant le niveau et le score pour ce niveau 
	 */
	public Joueur getJoueurScoreByLevel(int niveau) {
		PreparedStatement ps = null;
		ResultSet resultat = null;
		Joueur joueur = null;
		
		JoueurAuth ja = new JoueurAuth();
		String query = "SELECT j.idJoueur, nom, score FROM score sc \r\n" + 
				"INNER JOIN joueur j ON j.idJoueur = sc.idJoueur\r\n" + 
				"WHERE sc.niveau = ? AND sc.idJoueur = ? AND sc.idScore NOT IN(SELECT idScore FROM sauvegarde)";
		try {
			ps = connexion.prepareStatement(query);
			ps.setInt(1, niveau);
			ps.setInt(2, ja.getJoueurAuth().getIdJoueur());
			resultat = ps.executeQuery();
			while(resultat.next()) {
				joueur = rsetToJoueurScore(resultat);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeRqt(resultat, ps);
		}
		
		return joueur;
	}
	
	
	/** 
	 * Permets de retourner l'idScore du joueur connecté pour un certain niveau 
	 * @param niveau 
	 * @return int : idScore 
	 */
	private int getIdScoreJoueurByLevel(int niveau) {
		PreparedStatement ps = null;
		ResultSet resultat = null;

		JoueurAuth ja = new JoueurAuth();
		int score = 0;
		String query = "SELECT idScore FROM score sc \r\n" + 
				"INNER JOIN joueur j ON j.idJoueur = sc.idJoueur\r\n" + 
				"WHERE sc.niveau = ? AND sc.idJoueur = ?";
		try {
			ps = connexion.prepareStatement(query);
			ps.setInt(1, niveau);
			ps.setInt(2, ja.getJoueurAuth().getIdJoueur());

			resultat = ps.executeQuery();
			while(resultat.next()) {
				score = resultat.getInt("idScore");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeRqt(resultat, ps);
		}

		return score;
	}

	/**
	 * Permets de retourner l'idScore du joueur connecté par rapport à sa sauvegarde
	 * @return int : idScore
	 */
	private int getIdScoreBySauvegarde() {
		PreparedStatement ps = null;
		String query = "SELECT score.idScore FROM sauvegarde INNER JOIN score "
				+ "ON score.idScore = sauvegarde.idScore WHERE score.idJoueur = ?"; 
		ResultSet resultat = null;
		JoueurAuth ja = new JoueurAuth();
		int idScore = 0;
		try {
			ps = connexion.prepareStatement(query);
			ps.setInt(1, ja.getJoueurAuth().getIdJoueur());
			resultat = ps.executeQuery();
			while (resultat.next()) {
				idScore = resultat.getInt("idScore");
			}

		} catch (SQLException e) {
			System.out.println("Error query get idSauvegarde");
			e.printStackTrace();
		} finally {
			closeRqt(resultat,ps);
		}
		return idScore;
	}
	
	/**
	 * Permets de connaitre le Niveau sauvegardé (sa progression) pour le joueur connecté
	 * @return Niveau : niveau sauvegardé
	 */
	public Niveau getSave() {
		JoueurAuth joueurAuth = new JoueurAuth(); 	
		PreparedStatement ps = null;
		ResultSet resultat = null;
		String query = "SELECT niveau,score,sauvegarde FROM sauvegarde sa\r\n" + 
				"INNER JOIN score sc ON sc.idScore = sa.idScore\r\n" + 
				"WHERE sc.idJoueur = ?"; 
		Niveau n = new Niveau();
		try {
			ps = connexion.prepareStatement(query);
			ps.setInt(1, joueurAuth.getJoueurAuth().getIdJoueur());
			resultat = ps.executeQuery();

			while (resultat.next()) {
				n.setNumNiveau(resultat.getInt("niveau"));
				n.setScore(resultat.getInt("score"));
				n.setTerrain(resultat.getString("sauvegarde"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeRqt(resultat,ps);
		}
	
		return n;
	}
	
	/**
	 * Permets de sauvegarder le score d'un joueur 
	 * @param n : Niveau devant contenir niveau et score 
	 * @return boolean : true si cela a fonctionné false sinon
	 */
	public Boolean setScore(Niveau n) {
		JoueurAuth ja = new JoueurAuth(); 
		PreparedStatement ps = null;

		int idScore = getIdScoreJoueurByLevel(n.getNumNiveau());
		if(idScore == 0) {

			String query = "INSERT INTO score (score,niveau,idJoueur) " + "VALUES(?,?,?)";

			try {
				ps = connexion.prepareStatement(query);
				ps.setInt(1, n.getScore());
				ps.setInt(2, n.getNumNiveau());
				ps.setInt(3, ja.getJoueurAuth().getIdJoueur());
				ps.executeUpdate();
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}


		} else {
			String update = "UPDATE score SET score = ? WHERE idScore = ?";

			try {
				ps = connexion.prepareStatement(update);
				ps.setInt(1, n.getScore());
				ps.setInt(2, idScore);
				ps.executeUpdate();
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}

		}
		System.out.println("Score bien ajoutée");
		return true;
	}
	
	
	/**
	 * Permets d'enregistrer dans la base de données le score et la progression d'un joueur (un Niveau) 
	 * @param n : Niveau 
	 * @return boolean : true si cela a fonctionné; false sinon
	 */
	public Boolean setScoreAndSave(Niveau n) {
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		JoueurAuth ja = new JoueurAuth();

		int idScore = getIdScoreBySauvegarde();

		if(idScore == 0) {
			try {
				String query1 = "INSERT INTO score (score,niveau,idJoueur) " + "VALUES(?,?,?)";
				String query2 = "INSERT INTO sauvegarde (sauvegarde,idScore) " + "VALUES(?,?)";
				ps2 = connexion.prepareStatement(query1, Statement.RETURN_GENERATED_KEYS);
				ps2.setInt(1, n.getScore());
				ps2.setInt(2, n.getNumNiveau());
				ps2.setInt(3, ja.getJoueurAuth().getIdJoueur());
				ps2.executeUpdate();

				try (ResultSet generatedKeys2 = ps2.getGeneratedKeys()) {
					if (generatedKeys2.next()) {
						int lastInsertId2 = (int) generatedKeys2.getLong(1);
						ps = connexion.prepareStatement(query2);
						ps.setString(1, n.getTerrain());
						ps.setInt(2, lastInsertId2);

						ps.executeUpdate();
						ps.close();
						ps2.close();
					}
				} catch (SQLException e) {
					System.out.println("Error query 1");
					e.printStackTrace();
					return false;
				}

			} catch (SQLException e) {
				System.out.println("Error query 2");
				e.printStackTrace();
				return false;
			}
		} else {					
			String update = "UPDATE `sauvegarde` INNER JOIN score ON score.idScore = sauvegarde.idScore SET sauvegarde = ?, niveau = ?, score = ? WHERE score.idScore = ?";
			try {
				// UPDATE token
				ps = connexion.prepareStatement(update);

				ps.setString(1, n.getTerrain());
				ps.setInt(2, n.getNumNiveau());
				ps.setInt(3, n.getScore());
				ps.setInt(4, idScore);
				ps.executeUpdate();

				ps.close();
			} catch (SQLException e) {
				System.out.println("Error update sauvegarde");
				e.printStackTrace();
				return false;
			} 

		}

		System.out.println("Sauvegarde bien faite");
		return true;

	}
	
	
	/**
	 * Permets de récupérer l'idJoueur à partir du token 
	 * @param token : String 
	 * @return int : idJoueur
	 * @throws SQLException
	 */
	public int getJoueurToken(String token) throws SQLException {
		PreparedStatement ps = null;
		ResultSet resultat = null;
		String query = "SELECT idJoueur FROM joueur WHERE token = ?"; 
		int idJoueur = 0; 
		try {
			ps = connexion.prepareStatement(query);
			ps.setString(1, token);
			resultat = ps.executeQuery();
			while (resultat.next()) {
				idJoueur = resultat.getInt("idJoueur");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeRqt(resultat, ps);
		}
		return idJoueur;
	}
	
	
	/**
	 * Permets de mettre à jour dans la base de donnée le token du joueur connecté
	 * @param token : String : que l'on souhaite enregistré 
	 * @return boolean : True si cela a fonctionné, false sinon
	 */
	public boolean setToken(String token) {
		JoueurAuth joueurAuth = new JoueurAuth(); 	
		PreparedStatement ps = null; 
		String query = "UPDATE joueur SET token = ? WHERE idJoueur = ?"; 
		try {
			ps = connexion.prepareStatement(query);
			ps.setString(1, token);
			ps.setInt(2, joueurAuth.getJoueurAuth().getIdJoueur());
			ps.executeUpdate();

			ps.close();
			System.out.println("Token bien mis à jour");
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		return true;
	}
	
	
	/**
	 * Permets de sauvegarder le server
	 * @param n : Niveau devant contenir niveau et score 
	 * @return boolean : true si cela a fonctionné false sinon
	 * @throws SQLException 
	 */
	public int setServer(String serverIpLocal,String serverIpExt, String serverName) throws SQLException {

		PreparedStatement ps = null;

		int lastInsertId =0; 
		String query = "INSERT INTO server (ipLocal,ipExt,serverName,placeDispo) " + "VALUES(?,?,?,?)";

		try {
		
			ps = connexion.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, serverIpLocal);
			ps.setString(2, serverIpExt);
			ps.setString(3, serverName);
			ps.setInt(4, 2);
			
			int affectedRows = ps.executeUpdate();
			
			if (affectedRows == 0) {
				throw new SQLException("Creating cours failed, no rows affected.");
			}
			
			
			try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					lastInsertId = (int)generatedKeys.getLong(1);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			System.out.println("Serveur ajouté");
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		

		return lastInsertId;
		

	}



	public void setNbrPlaceDisponible(int placeDispo, int idServer) {
		PreparedStatement ps = null; 
		String query = "UPDATE server SET placeDispo = ? WHERE idServer = ?"; 
		try {
			ps = connexion.prepareStatement(query);
			ps.setInt(1, placeDispo);
			ps.setInt(2, idServer);
			ps.executeUpdate();

			ps.close();
			System.out.println("MAJ - nbr place disponible : " + placeDispo);
		} catch (SQLException e) {
			e.printStackTrace();
		} 
	}
	
	public void deleteServer(int idServer) {
		PreparedStatement ps = null; 
		String query = "DELETE FROM server WHERE idServer = ?"; 
		try {
			ps = connexion.prepareStatement(query);
			ps.setInt(1, idServer);
			ps.executeUpdate();

			ps.close();
			System.out.println("Server supprimé");
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		
	}
	
	
	
	
	/**
	 * Permets de fermer une requête
	 * @param resultat : ResultSet
	 * @param statement : PreparedStatement
	 */
	private void closeRqt(ResultSet resultat, Statement statement) {
		if (resultat != null) {
			try {
				resultat.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	private void closeRqt(ResultSet resultat, PreparedStatement statement) {
		if (resultat != null) {
			try {
				resultat.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Permets de fermer proprement la connexion
	 */
	public void closeGestionRqt() {
		if (this.connexion != null) {
			try {
				connexion.close();
				System.out.println("Connexion à la base de données fermée");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
