package connexion;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;


import ctf.TerrainCtf;
import javafx.application.Platform;

import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import javafx.stage.Stage;

public class ConnexionServer {

	static Socket socket;
	static DataInputStream dIn; 
	static DataOutputStream dOut; 
	static Thread t;

	Stage stage; 
	Button sendBtn; 
	TextArea ta; 
	TextField tf; 



	static GetNewMsg getMsg;	

	public ConnexionServer(String ipLocal, String ipExt) throws IOException {

		boolean coExtReussi;

		int serverPort = 5656;
		//InetAddress inetAdd = InetAddress.getByName(ip);
		InetAddress inetAdd = InetAddress.getByName("192.168.1.69");
		try {
			socket = new Socket(inetAdd, serverPort);
			System.out.println("Connexion au serveur établi");

			InputStream in = socket.getInputStream();
			OutputStream out = socket.getOutputStream();

			dIn = new DataInputStream(in);
			dOut = new DataOutputStream(out);


			getMsg = new GetNewMsg(dIn);
			t = new Thread(getMsg);
			t.start();

			coExtReussi = true;

		}
		catch (Exception e) { 
			coExtReussi = false;	
		}

		if(!coExtReussi) {

			inetAdd = InetAddress.getByName(ipExt);
			try {
				socket = new Socket(inetAdd, serverPort);
				System.out.println("Connexion au serveur établi");

				InputStream in = socket.getInputStream();
				OutputStream out = socket.getOutputStream();

				dIn = new DataInputStream(in);
				dOut = new DataOutputStream(out);


				getMsg = new GetNewMsg(dIn);
				t = new Thread(getMsg);
				t.start();



			} catch (Exception e) { 

			}
		}



	}


	public void sendMessage(int n ) throws IOException {
		dOut.writeInt(n);
		dOut.flush();
	}

	public void exit() throws IOException {
		sendMessage(999);
		
		try {
			Thread.sleep(100);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		socket.shutdownInput();
		
		dOut.close();




		t.interrupt(); 
		try {
			t.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		if(t.isInterrupted()) System.out.println("Fin thread");


		dIn.close();
		socket.close();
	
	}
}

class GetNewMsg implements Runnable {
	DataInputStream dIn;
	private boolean running = true;


	GetNewMsg(DataInputStream dIn) throws IOException {
		this.dIn = dIn;

	}

	public void terminate() {
		System.out.println("Termine");
		running = false;
	}

	@Override
	public void run() {
		int mouvement = 0;



		while (running) {
			try {
				mouvement = dIn.readInt();
				System.out.println("Nouveau mouvement : " + mouvement);
				if(mouvement < 10) {					
					TerrainCtf.moveJoueurAdverse(mouvement);
				} else {
					int mouvementFinal = mouvement; 
					Platform.runLater(new Runnable() {
						@Override public void run() {

							TerrainCtf.tirJoueurAdverse(mouvementFinal);
							// etc
						}
					});
				} 
				if(Thread.interrupted()) {
					running = false;
				}

			} catch (IOException e) {

				running = false;
			} 
		}

		System.out.println("Sortie de boucle");
	
	}


}
