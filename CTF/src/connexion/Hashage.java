package connexion;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hashage {
	
	private String psd; 
	
	public Hashage(String password) {
		this.psd = password;
	}
  
  	
	/**
	 * Hash le string mis en paramètre dans le constructeur en SHA-256 
	 * @return String : le hash du string mis en paramètre dans le constructeur
	 * @throws NoSuchAlgorithmException
	 */
	public String hashString() throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(psd.getBytes());

		byte byteData[] = md.digest();

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}

		return sb.toString();

	}
	
	
}