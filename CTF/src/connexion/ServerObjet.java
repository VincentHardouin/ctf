package connexion;

public class ServerObjet {
	
	private String ipLocal;
	private String ipExt;
	private String serverName; 
	private int nbrPlaceDispo; 
	
	public ServerObjet() {
		
	}

	

	public String getIpLocal() {
		return ipLocal;
	}



	public void setIpLocal(String ipLocal) {
		this.ipLocal = ipLocal;
	}



	public String getIpExt() {
		return ipExt;
	}



	public void setIpExt(String ipExt) {
		this.ipExt = ipExt;
	}



	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public int getNbrPlaceDispo() {
		return nbrPlaceDispo;
	}

	public void setNbrPlaceDispo(int nbrPlaceDispo) {
		this.nbrPlaceDispo = nbrPlaceDispo;
	}
	
	
	
	
}
